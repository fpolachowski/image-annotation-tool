import config
import pygame
import sys
import os
from glob import glob
import json
from copy import deepcopy


from Objects import Cube, Square, Button, Label, Image, LinkedIter
from Dialogs import FileChooser, CommentDialog, DownloadDialog, SettingsDialog


class Game():
    def __init__(self):
        self.cfg = config.getConfig()
        pygame.init()
        self.screen = pygame.display.set_mode(
            self.cfg.MAIN.SCREEN, pygame.RESIZABLE)

        pygame.display.set_caption("Revision System")

        # - objects -
        self.cubes = []
        self.squares = []

        self.image = None
        self.imageNames = []
        self.files = []

        self.load_Images(path=self.cfg.FOLDER)
        if os.path.isfile('progress.json'):
            self.loadProgress()

        self.state = 'squares'  # 'cubes'

        self.loadLayout()

        self.clock = pygame.time.Clock()

        self.running = True

    def loadLayout(self, new_dim=None):
        if new_dim is not None:
            old_dim = self.cfg.MAIN.SCREEN
            config.update(new_dim)
            self.cfg = config.getConfig()

            for square in self.squares:
                square.rescale(old_dim, new_dim)
            for cube in self.cubes:
                cube.rescale(old_dim, new_dim)

            if self.image is not None:
                self.image.loadImage(new_dim)

        # - create layout - #
        pos = self.cfg.POSITIONS
        self.labels = []
        self.labels.append(Label('Cube amount: %s' % len(
            self.squares), pos.LABEL_CUBES))

        self.buttons = []
        button_plus = Button(
            '+', pos.BUTTON_PLUS, self.cfg.SHAPES.BUTTON_SQUARE, '+1')
        self.buttons.append(button_plus)
        button_minus = Button(
            '-', pos.BUTTON_MINUS, self.cfg.SHAPES.BUTTON_SQUARE, '-1')
        self.buttons.append(button_minus)
        button_location = Button(
            'Open Files', pos.BUTTON_OPEN, self.cfg.SHAPES.BUTTON_LONG, 'open')
        self.buttons.append(button_location)
        button_prev = Button(
            'Previous', pos.BUTTON_PREV, self.cfg.SHAPES.BUTTON_LONG, 'prev')
        self.buttons.append(button_prev)
        button_next = Button(
            'Next', pos.BUTTON_NEXT, self.cfg.SHAPES.BUTTON_LONG, 'next')
        self.buttons.append(button_next)
        button_help = Button(
            'Comment', pos.BUTTON_COMMENT, self.cfg.SHAPES.BUTTON_LONG, 'error')
        self.buttons.append(button_help)
        button_exit = Button(
            'EXIT', pos.BUTTON_EXIT, self.cfg.SHAPES.BUTTON_LONG, 'exit', color=self.cfg.MAIN.RED)
        self.buttons.append(button_exit)
        button_reset = Button(
            'Reset', pos.BUTTON_RESET, self.cfg.SHAPES.BUTTON_LONG, 'reset', color=self.cfg.MAIN.RED)
        self.buttons.append(button_reset)

    def quit(self):
        self.running = False
        pygame.display.quit()
        pygame.quit()
        sys.exit()
        quit()

    def run(self):
        while self.running:

            # - events -

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        for cube in self.cubes:
                            cube.checkCollision(event.pos)
                        for square in self.squares:
                            square.checkCollision(event.pos)
                        for button in self.buttons:
                            if button.checkCollision(event.pos):
                                self.handleFunction(button.function())

                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        for cube in self.cubes:
                            cube.mouseUp()
                        for square in self.squares:
                            square.mouseUp()

                elif event.type == pygame.MOUSEMOTION:
                    for cube in self.cubes:
                        cube.move(event.pos)
                    for square in self.squares:
                        square.move(event.pos)

                elif event.type == pygame.VIDEORESIZE:
                    self.screen = pygame.display.set_mode((event.w, event.h),
                                                          pygame.RESIZABLE)
                    self.loadLayout((event.w, event.h))

            # - draws (without updates) -

            amount = len(self.squares) if len(self.squares) > len(
                self.cubes) else len(self.cubes)

            self.labels[0].set_Text('Cube amount: %s' % amount)
            self.draw()

            # - constant game speed / FPS -

            self.clock.tick(self.cfg.MAIN.FPS)

        self.quit()

    def draw(self):
        self.screen.fill(self.cfg.MAIN.WHITE)

        if self.image is not None:
            self.screen.blit(self.image.img, self.cfg.POSITIONS.IMAGE)

        for button in self.buttons:
            pygame.draw.rect(self.screen, button.color, button.button)
            self.screen.blit(button.label.label, button.label.label_rect)

        for label in self.labels:
            self.screen.blit(label.label, label.label_rect)

        if(self.state == 'cubes'):
            for cube in self.cubes:
                cube.draw(self.screen)
        else:
            for square in self.squares:
                square.draw(self.screen)

        pygame.display.flip()

    def handleFunction(self, function):
        if function == 'open':
            self.open()
        elif function == '+1':
            self.squares.append(
                Square(self.cfg.POSITIONS.SQUARE, len(self.squares) + 1))
        elif function == '-1':
            if len(self.squares) > 0:
                del self.squares[-1]
        elif function == 'next':
            self.nextIteration()
        elif function == 'prev' and (self.image_iterator.index > 0 or self.state == 'cubes'):
            self.prevIteration()
        elif function == 'error' and self.image is not None:
            self.error()
        elif function == 'exit':
            self.saveProgress()
            self.quit()
        elif function == 'reset':
            self.loadShapes(reset=True)

    def open(self):
        if(os.path.isdir(self.cfg.FOLDER)):
            fileChooser = FileChooser()
            files = fileChooser.OpenFile(
                os.path.dirname(os.path.abspath(__file__)))
            fileChooser.destroy()
            if files is not None:
                self.files = []
                for f in files:
                    if '.json' in f:
                        self.files.append(f)
                self.loadShapes()
        else:
            dw_dialog = DownloadDialog()
            if dw_dialog.status == 'finished':
                self.load_Images(path=self.cfg.FOLDER)

    def error(self):
        dialog = CommentDialog(self.image.message)
        self.image.message = dialog.getText()

    def saveProgress(self):
        if self.image is None:
            return
        data = {}
        data['visualizer'] = {
            'currentState': self.state,
            'currentImage': self.image.fileName
        }
        with open('progress.json', 'w') as f:
            json.dump(data, fp=f, indent=5)

    def loadProgress(self):
        with open('progress.json') as f:
            data = json.load(f)
            if 'visualizer' in data:
                self.state = data['visualizer']['currentState']
                currentImage = data['visualizer']['currentImage']
                self.load_Images(self.cfg.FOLDER)
                while self.image.fileName != currentImage:
                    self.loadImage()

    def load_Images(self, path):
        self.images = glob(path + "/*.png")
        self.images.extend(glob(path + "/*.jpg"))
        self.image_iterator = LinkedIter(self.images)
        print(self.images)
        try:
            self.loadImage()
        except StopIteration:
            pass

    def loadImage(self, forward=True):
        if forward:
            image, index = next(self.image_iterator)
        else:
            image, index = self.image_iterator.prev()
        self.image = Image(image, index)
        self.image.loadImage(self.cfg.MAIN.SCREEN)
        self.loadShapes()
        return index

    def loadShapes(self, reset=False):
        if reset:
            self.squares = deepcopy(self.squares)
            self.cubes = deepcopy(self.cubes)
        for f in self.files:
            if self.image.loadFromFile(self.cfg.MAIN.SCREEN, fileName=f):
                break

        if reset:
            if self.state == 'squares':
                self.squares = self.image.squares
            else:
                self.cubes = self.image.cubes
        else:
            self.squares = self.image.squares
            self.cubes = self.image.cubes

    def prevIteration(self):
        try:
            if self.state == 'squares':
                self.loadImage(forward=False)
            else:
                self.state = 'squares'
        except StopIteration:
            pass

    def nextIteration(self):
        try:
            if self.state == 'cubes':
                self.image.writeToDict_app(
                    self.cubes, self.cfg.MAIN.SCREEN, fileName=self.cfg.FINAL_FILE)
                self.loadImage()
            self.state = 'cubes' if self.state == 'squares' else 'squares'
        except StopIteration:
            self.running = False


Game().run()
