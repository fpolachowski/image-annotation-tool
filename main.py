import pygame
from glob import glob
from csv import writer
import json

from Objects import Cube, Square, Button, Label, Image, LinkedIter
from Dialogs import FolderChooser, CommentDialog, DownloadDialog, SettingsDialog
import os
import sys
import time

# --- constants --- (UPPER_CASE names)

# BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED = (255,   0,   0)

FPS = 30


class Game():
    def __init__(self):
        self.config()
        pygame.init()
        self.screen = pygame.display.set_mode(
            (self.SCREEN_WIDTH, self.SCREEN_HEIGHT), pygame.RESIZABLE)

        pygame.display.set_caption("Labeling System")

        # - objects -
        self.cubes = []
        self.squares = [Square(self.SQUARE_POS, 1)]

        self.imageModule = None
        self.image_location = './images'
        self.images = []
        # Bounding, Annotation without Boundingbox, annotation_wb with Boundingbox
        self.game_state = 'File Selection'
        self.editorNo = -1

        if os.path.isfile('progress.json'):
            self.loadProgress()

        self.loadLayout(self.SCREEN_WIDTH, self.SCREEN_HEIGHT)
        self.startTime = time.time()

        self.clock = pygame.time.Clock()

        self.running = True

    def config(self):
        self.SCREEN_WIDTH = 800
        self.SCREEN_HEIGHT = 800
        self.BUTTON_SQUARE = (40, 40)
        self.BUTTON_LONG = (120, 40)

        self.SQUARE_POS = (self.SCREEN_WIDTH/2, self.SCREEN_HEIGHT/4)
        self.CUBE_POS = (self.SCREEN_WIDTH/4, self.SCREEN_HEIGHT/4)
        self.HEADER_POS = (self.SCREEN_WIDTH/8, self.SCREEN_HEIGHT/20)
        self.IMAGE_POS = (0, self.SCREEN_HEIGHT/8)

    def loadLayout(self, SCREEN_WIDTH, SCREEN_HEIGHT):
        for square in self.squares:
            square.rescale((self.SCREEN_WIDTH, self.SCREEN_HEIGHT),
                           (SCREEN_WIDTH, SCREEN_HEIGHT))
        for cube in self.cubes:
            cube.rescale((self.SCREEN_WIDTH, self.SCREEN_HEIGHT),
                         (SCREEN_WIDTH, SCREEN_HEIGHT))

        # - renew game values - #
        self.SQUARE_POS = (self.SCREEN_WIDTH/2, self.SCREEN_HEIGHT/4)
        self.CUBE_POS = (self.SCREEN_WIDTH/4, self.SCREEN_HEIGHT/4)
        self.HEADER_POS = (SCREEN_WIDTH/8, SCREEN_HEIGHT/20)
        self.IMAGE_POS = (0, SCREEN_HEIGHT/8)
        self.SCREEN_WIDTH = SCREEN_WIDTH
        self.SCREEN_HEIGHT = SCREEN_HEIGHT

        if self.images != [] and self.img is not None:
            self.img = self.imageModule.loadImage(
                (SCREEN_WIDTH, SCREEN_HEIGHT))

        # - create layout - #
        self.labels = []
        self.labels.append(Label('Cube amount: %s' % len(
            self.squares), (self.HEADER_POS[0], self.HEADER_POS[1] * 1.5)))
        self.labels.append(
            Label('Process: %s' % self.game_state, (self.HEADER_POS[0] * 4 - 5, self.HEADER_POS[1] * 1.5)))

        self.buttons = []
        button_plus = Button(
            '+', (self.HEADER_POS[0] * 2, self.HEADER_POS[1]), self.BUTTON_SQUARE, '+1')
        self.buttons.append(button_plus)
        button_minus = Button(
            '-', (self.HEADER_POS[0] * 2 + self.BUTTON_SQUARE[0] + 10, self.HEADER_POS[1]), self.BUTTON_SQUARE, '-1')
        self.buttons.append(button_minus)
        button_location = Button(
            'Open Files', (self.HEADER_POS[0] - self.labels[0].getWidth()/2, self.HEADER_POS[1] - 45), self.BUTTON_LONG, 'open')
        self.buttons.append(button_location)
        button_prev = Button(
            'Previous', (self.HEADER_POS[0] * 5, self.HEADER_POS[1]), self.BUTTON_LONG, 'prev')
        self.buttons.append(button_prev)
        button_next = Button(
            'Next', (self.HEADER_POS[0] * 5 + self.BUTTON_LONG[0] + 10, self.HEADER_POS[1]), self.BUTTON_LONG, 'next')
        self.buttons.append(button_next)
        button_help = Button(
            'Comment', (self.HEADER_POS[0] * 5 - self.BUTTON_LONG[0] - 10, self.HEADER_POS[1] - 45), self.BUTTON_LONG, 'error')
        self.buttons.append(button_help)
        button_exit = Button(
            'EXIT', (self.HEADER_POS[0] * 5 + self.BUTTON_LONG[0] + 10, self.HEADER_POS[1] - 45), self.BUTTON_LONG, 'exit', color=RED)
        self.buttons.append(button_exit)
        button_editor = Button(
            'Settings', (self.HEADER_POS[0] - self.labels[0].getWidth()/2 + self.BUTTON_LONG[0] + 10, self.HEADER_POS[1] - 45), self.BUTTON_LONG, 'edit')
        self.buttons.append(button_editor)
        button_reset = Button(
            'Reset', (self.HEADER_POS[0] * 5, self.HEADER_POS[1] - 45), self.BUTTON_LONG, 'reset', color=(RED))
        self.buttons.append(button_reset)

    def load_Images(self):
        if self.image_location is not None:
            self.images = glob(self.image_location + "/*.png")
            self.images.extend(glob(self.image_location + "/*.jpg"))
            self.image_iterator = LinkedIter(self.images)
            self.loadImage()

    def saveProgress(self):
        if self.imageModule is None:
            return
        data = {}
        data['state'] = {
            'currentState': self.game_state,
            'currentImage': self.imageModule.fileName,
            'image_location': self.image_location,
            'editorNo': self.editorNo
        }
        with open('progress.json', 'w') as f:
            json.dump(data, fp=f, indent=5)

    def saveCurrentState(self):
        if self.imageModule is not None:
            found = False
            if os.path.isfile(r'state.json'):
                with open(r'state.json', 'r') as f:
                    images = json.load(f)
                    for i in range(len(images)):
                        if images[i]['fileName'] == self.imageModule.fileName:
                            images[i] = self.imageModule.toDict(
                                (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                            found = True
                            break

            if found:
                with open(r'state.json', 'wb') as f:
                    f.write('['.encode())
                    for _dict in images:
                        f.write(json.dumps(_dict).encode())
                        f.write('\n'.encode())
                        f.write(' , '.encode())
                    f.seek(-2, 2)
                    f.truncate()
                    f.write(']'.encode())
            else:
                _dict = self.imageModule.toDict(
                    (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
                with open(r'state.json', 'ab+') as f:
                    f.seek(0, 2)  # Go to the end of file
                    if f.tell() == 0:  # Check if file is empty
                        # If empty, write an array
                        f.write('['.encode())
                        f.write(json.dumps(_dict).encode())
                        f.write('\n'.encode())
                        f.write(']'.encode())
                    else:
                        f.seek(-1, 2)
                        f.truncate()  # Remove the last character, open the array
                        f.write(' , '.encode())  # Write the separator
                        # Dump the dictionary
                        f.write(json.dumps(_dict).encode())
                        f.write('\n'.encode())
                        f.write(']'.encode())  # Close the array

    def loadProgress(self):
        with open('progress.json') as f:
            data = json.load(f)
            if 'state' is data:
                self.game_state = data['state']['currentState']
                currentImage = data['state']['currentImage']
                self.image_location = data['state']['image_location']
                if 'editorNo' in data['state']:
                    self.editorNo = data['state']['editorNo']
                self.load_Images()
                while self.imageModule.fileName != currentImage:
                    self.loadImage()

    def handleFunction(self, function):
        if function == '+1' and self.game_state == 'Bounding':
            self.squares.append(
                Square((self.SQUARE_POS[0] + len(self.squares) * 13, self.SQUARE_POS[1] + len(self.squares) * 17), len(self.squares) + 1))
        elif function == '-1' and self.game_state == 'Bounding':
            if len(self.squares) > 0:
                del self.squares[-1]
        elif function == 'next' and self.game_state != 'File Selection':
            self.nextIteration()
        elif function == 'prev' and self.game_state != 'File Selection' and self.image_iterator.index > 0:
            self.prevIteration()
        elif function == 'open':
            self.open()
        elif function == 'error' and self.game_state != 'File Selection' and self.imageModule is not None:
            self.error()
        elif function == 'exit':
            self.saveProgress()
            self.quit()
        elif function == 'edit':
            self.openSettings()
        elif function == 'reset':
            self.reset()

    def run(self):
        while self.running:

            # - events -

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.saveProgress()
                    self.quit()

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        for cube in self.cubes:
                            cube.checkCollision(event.pos)
                        for square in self.squares:
                            square.checkCollision(event.pos)
                        for button in self.buttons:
                            if button.checkCollision(event.pos):
                                self.handleFunction(button.function())

                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        for cube in self.cubes:
                            cube.mouseUp()
                        for square in self.squares:
                            square.mouseUp()

                elif event.type == pygame.MOUSEMOTION:
                    for cube in self.cubes:
                        cube.move(event.pos)
                    for square in self.squares:
                        square.move(event.pos)

                elif event.type == pygame.VIDEORESIZE:
                    self.screen = pygame.display.set_mode((event.w, event.h),
                                                          pygame.RESIZABLE)
                    self.loadLayout(event.w, event.h)

            # - draws (without updates) -

            amount = len(self.squares) if len(self.squares) > len(
                self.cubes) else len(self.cubes)

            self.labels[0].set_Text('Cube amount: %s' % amount)
            self.draw()

            # - constant game speed / FPS -

            self.clock.tick(FPS)

        self.quit()

    def quit(self):
        self.running = False
        pygame.display.quit()
        pygame.quit()
        sys.exit()
        quit()

    def loadImage(self, forward=True):
        if forward:
            image, index = next(self.image_iterator)
        else:
            image, index = self.image_iterator.prev()
        self.imageModule = Image(image, index)
        self.imageModule.loadFromFile((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        self.img = self.imageModule.loadImage(
            (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        self.startTime = time.time()

        if(self.game_state != 'Annotation' and self.imageModule.squares != []):
            self.squares = self.imageModule.squares

        if(self.game_state == 'Annotation' or self.game_state == 'Annotation +'):
            self.cubes = []
            for i in range(len(self.imageModule.squares)):
                self.cubes.append(
                    Cube((self.CUBE_POS[0] + i * 25, self.CUBE_POS[1] + i * 25), len(self.cubes) + 1))

        return index

    def open(self):
        if(os.path.isdir('./images')):
            folderChooser = FolderChooser()
            self.image_location = folderChooser.OpenFile(
                os.path.dirname(os.path.abspath(__file__)))
            folderChooser.destroy()
            if self.image_location is not None and self.image_location != '':
                self.load_Images()
                self.game_state = 'Bounding'
        else:
            dw_dialog = DownloadDialog()
            if dw_dialog.status == 'finished':
                self.image_location = './images'
                self.load_Images()
                self.game_state = 'Bounding'

    def reset(self):
        self.squares = [
            Square(self.SQUARE_POS, 1)]

        if(self.game_state != 'Annotation' and self.imageModule.squares != []):
            self.squares = self.imageModule.squares

        if(self.game_state == 'Annotation' or self.game_state == 'Annotation +'):
            self.cubes = []
            for i in range(len(self.imageModule.squares)):
                self.cubes.append(
                    Cube((self.CUBE_POS[0] + i * 25, self.CUBE_POS[1] + i * 25), len(self.cubes) + 1))

    def openSettings(self):
        correct = False
        dialog = SettingsDialog()
        while not correct:
            try:
                num = int(dialog.getText())
                self.editorNo = num
                correct = True
            except:
                dialog = SettingsDialog(error=True)

    def error(self):
        dialog = CommentDialog(self.imageModule.message)
        self.imageModule.message = dialog.getText()

    def prevIteration(self):

        if self.editorNo != -1:
            found = False
            while not found:
                index = self.loadImage(forward=False)
                if index % 4 == self.editorNo:
                    found = True
        else:
            self.loadImage(forward=False)

        if(self.game_state == 'Bounding' or self.game_state == 'Annotation +'):
            self.squares = self.imageModule.squares
            for square in self.squares:
                square.static = False
        if(self.game_state == 'Annotation' or self.game_state == 'Annotation +'):
            self.cubes = self.imageModule.cubes

    def nextIteration(self):
        if self.imageModule is not None:
            self.imageModule.time += time.time() - self.startTime
        if(self.game_state == 'Bounding'):
            self.imageModule.saveSquare(self.squares)
            self.saveCurrentState()
            self.squares = [
                Square(self.SQUARE_POS, 1)]
        elif(self.game_state == 'Annotation'):
            self.imageModule.saveCube(self.cubes)
            self.saveCurrentState()
            self.cubes = []
        elif(self.game_state == 'Annotation +'):
            self.imageModule.writeToDict_app(
                self.cubes, (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
            self.cubes = []

        try:
            if self.editorNo != -1:
                found = False
                while not found:
                    index = self.loadImage()
                    if index % 4 == self.editorNo:
                        found = True
            else:
                self.loadImage()

        except StopIteration:
            if(self.game_state == 'Bounding'):
                self.game_state = 'Annotation'
                self.labels[1].set_Text('Process: Annotation')
                self.image_iterator = LinkedIter(self.images)
                self.loadImage()
            elif(self.game_state == 'Annotation'):
                self.game_state = 'Annotation +'
                self.labels[1].set_Text('Process: Annotation +')
                self.image_iterator = LinkedIter(self.images)
                self.loadImage()
            else:
                self.running = False

    def draw(self):
        self.screen.fill(WHITE)

        if self.images != [] and self.img is not None:
            self.screen.blit(self.img, self.IMAGE_POS)

        for button in self.buttons:
            pygame.draw.rect(self.screen, button.color, button.button)
            self.screen.blit(button.label.label, button.label.label_rect)

        for label in self.labels:
            self.screen.blit(label.label, label.label_rect)

        if(self.game_state == 'Annotation' or self.game_state == 'Annotation +'):
            for cube in self.cubes:
                cube.draw(self.screen)
        if(self.game_state == 'Bounding' or self.game_state == 'Annotation +'):
            for square in self.squares:
                square.draw(self.screen)

        pygame.display.flip()


game = Game()
game.run()
