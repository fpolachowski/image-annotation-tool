from threading import Thread

import os
import requests
import sys
import zipfile
import shutil
import glob
import time


class Service(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.status = 'Waiting for download to start.'
        self.done = 0
        self.waiting = True
        self.error = False
        self.start()

    def run(self):
        while self.waiting:
            time.sleep(0)
        self.startDownLoad()

    def startDownLoad(self):
        self.download()
        if self.error:
            self.status = 'Error'
            return
        self.unzip()
        if self.error:
            self.status = 'Error'
            return
        self.findFiles()
        self.deleteFiles()
        self.status = 'finished'

    def download(self):
        url = 'https://vision.cs.princeton.edu/projects/2012/SUNprimitive/data_release.zip'
        self.file_name = 'data_release.zip'

        if not os.path.isfile(self.file_name) and not os.path.isdir('data_release'):
            with open(self.file_name, "wb") as f:
                self.status = "Downloading %s" % self.file_name
                response = requests.get(url, stream=True)
                total_length = response.headers.get('content-length')

                if total_length is None:  # no content length header
                    f.write(response.content)
                else:
                    dl = 0
                    total_length = int(total_length)
                    for data in response.iter_content(chunk_size=4096):
                        dl += len(data)
                        f.write(data)
                        self.done = int(50 * dl / total_length)
                        if self.error:
                            break

    def unzip(self):
        if not os.path.isdir('images/') and os.path.isfile(self.file_name):
            self.status = 'Unzipping files...'
            with zipfile.ZipFile(self.file_name, 'r') as zf:
                self.status = "Unzipping  %s" % self.file_name
                uncompress_size = sum(
                    (file.file_size for file in zf.infolist()))

                extracted_size = 0

                for file in zf.infolist():
                    extracted_size += file.file_size
                    self.done = int(extracted_size * 50/uncompress_size)
                    zf.extract(file, 'images/')
                    if self.error:
                        break

    def findFiles(self):
        if os.path.isdir('images/'):
            self.status = "Moving files..."
            files = glob.glob(
                'images/data_release/cuboid/Images/internet/*/*.jpg')
            files.extend(
                glob.glob('images/data_release/cuboid/Images/misc/hand_captured/*.jpg'))
            for f in files:
                shutil.move(src=f, dst='images/')
        elif os.path.isdir('data_release/'):
            files = glob.glob(
                'data_release/cuboid/Images/internet/*/*.jpg')
            files.extend(
                glob.glob('data_release/cuboid/Images/misc/hand_captured/*.jpg'))
            if not os.path.isdir('images'):
                os.mkdir('images')
            for f in files:
                shutil.move(src=f, dst='images/')

    def deleteFiles(self):
        self.status = "Cleaning up..."
        if os.path.isdir('images/__MACOSX'):
            shutil.rmtree('images/__MACOSX')
        if os.path.isdir('images/data_release'):
            shutil.rmtree('images/data_release')
        if os.path.isfile('data_release.zip'):
            os.remove('data_release.zip')
        if os.path.isdir('data_release'):
            shutil.rmtree('data_release')
        if os.path.isdir('__MACOSX'):
            shutil.rmtree('__MACOSX')
