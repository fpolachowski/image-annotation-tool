certifi==2020.6.20
cffi==1.14.0
future==0.18.2
pygame==1.9.6
tkinter==8.6
requests==2.24.0
