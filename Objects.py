import pygame
import json
from copy import deepcopy
import os


class LinkedIter():
    def __init__(self, collection):
        self.collection = collection
        self.index = -1

    def __next__(self):
        try:
            self.index += 1
            result = self.collection[self.index]
        except IndexError:
            raise StopIteration
        return result, self.index

    def prev(self):
        self.index -= 1
        if self.index < 0:
            raise StopIteration
        return self.collection[self.index], self.index


class Button():
    def __init__(self, text, pos, dim, function, color=(200, 200, 200)):
        self.button = pygame.rect.Rect(pos[0], pos[1], dim[0], dim[1])
        self.label = Label(text, (pos[0] + dim[0]/2, pos[1] + dim[1]/2))
        self.color = color
        self.func = function

    def function(self):
        return self.func

    def checkCollision(self, event_pos):
        return self.button.collidepoint(event_pos)


class Label():
    def __init__(self, text, pos):
        font = pygame.font.SysFont('timesnewroman', 20)
        self.label, self.label_rect = self.text_objects(text, font)
        self.label_rect.center = pos

    def set_Text(self, text):
        font = pygame.font.SysFont('timesnewroman', 20)
        self.label, _ = self.text_objects(text, font)

    def text_objects(self, text, font):
        textSurface = font.render(text, True, (0, 0, 0))
        return textSurface, textSurface.get_rect()

    def getWidth(self):
        return self.label_rect.w


class Image():
    def __init__(self, fileName, index):
        self.fileName = fileName
        self.img = None
        self.squares = []
        self.cubes = []
        self.scale = [0, 0]
        self.time = 0.
        self.message = None
        self.index = index

    def loadFromFile(self, SCREEN_DIM, fileName='state.json'):
        if os.path.isfile(fileName):
            with open(fileName) as f:
                images = json.load(f)
                for image in images:
                    if image['fileName'] in self.fileName:
                        self.fromDict(image, SCREEN_DIM)
                        return True
        return False

    def loadImage(self, dim):
        image = pygame.image.load(self.fileName)
        image = self.resizeImage(image, dim)
        self.img = image
        return image

    def resizeImage(self, image, screen_dim):
        image_rect = image.get_rect()
        old_dim = (image_rect.w, image_rect.h)
        image = pygame.transform.scale(
            image, (int(screen_dim[0]), int(screen_dim[1] - screen_dim[1]/8)))
        image_rect = image.get_rect()
        new_dim = (image_rect.w, image_rect.h)
        self.scale = [new_dim[0] / old_dim[0], new_dim[1] / old_dim[1]]
        return image

    def saveCube(self, cubes):
        self.cubes = deepcopy(cubes)

    def saveSquare(self, squares):
        for square in squares:
            square.static = True
        self.squares = deepcopy(squares)

    def toDict(self, SCREEN_DIM):
        squares = []
        for square in self.squares:
            squares.append(square.toDict(SCREEN_DIM))
        cubes = []
        for cube in self.cubes:
            cubes.append(cube.toDict(SCREEN_DIM))
        return {
            'fileName': self.fileName,
            'squares': squares,
            'cubes': cubes,
            'scale': self.scale,
            'time': self.time,
            'message': self.message
        }

    def fromDict(self, dict, SCREEN_DIM):
        self.scale = dict["scale"]
        self.message = dict["message"]
        self.time = dict["time"]

        self.squares = []
        for square in dict["squares"]:
            new_sq = Square((0, 0), len(self.squares) + 1)
            new_sq.fromDict(square, SCREEN_DIM)
            self.squares.append(new_sq)

        self.cubes = []
        for cube in dict["cubes"]:
            new_cu = Cube((0, 0), len(self.cubes) + 1)
            new_cu.fromDict(cube, SCREEN_DIM)
            self.cubes.append(new_cu)

    def writeToDict_app(self, cubes, SCREEN_DIM, fileName='annotations.json'):
        list_names = self.fileName.split('/')
        name = list_names[-1].split('\\')
        file_name = name[-1]
        cubes_sec = []
        for cube in cubes:
            cubes_sec.append(cube.toDict(SCREEN_DIM))
        _dict = self.toDict(SCREEN_DIM)
        _dict['fileName'] = file_name
        _dict['cubes_sec_ann'] = cubes_sec

        with open(fileName, 'ab+') as f:
            f.seek(0, 2)  # Go to the end of file
            if f.tell() == 0:  # Check if file is empty
                # If empty, write an array
                f.write('['.encode())
                f.write(json.dumps(_dict).encode())
                f.write('\n'.encode())
                f.write(']'.encode())
            else:
                f.seek(-1, 2)
                f.truncate()  # Remove the last character, open the array
                f.write(' , '.encode())  # Write the separator
                f.write(json.dumps(_dict).encode())  # Dump the dictionary
                f.write('\n'.encode())
                f.write(']'.encode())  # Close the array


class Rect():
    def __init__(self, pos, dim, color):
        self.rect = pygame.rect.Rect(pos[0], pos[1], dim, dim)
        self.draging = False
        self.offset = (0, 0)
        self.color = color
        self.dim = dim
        self.x = self.rect.x
        self.y = self.rect.y

    def collidepoint(self, event_pos):
        if self.rect.collidepoint(event_pos):
            self.draging = True
            mouse_x, mouse_y = event_pos
            self.offset = (self.rect.x - mouse_x, self.rect.y - mouse_y)
            return [self.rect.x, self.rect.y]

    def move(self, event_pos):
        if self.draging:
            self.x = event_pos[0] + self.offset[0]
            self.y = event_pos[1] + self.offset[1]
            self.update()
            return [self.rect.x, self.rect.y]

    def update(self):
        self.rect.x = self.x
        self.rect.y = self.y

    def draw(self, screen):
        pos = (int(self.x + self.rect.w/2),
               int(self.y + self.rect.h/2))
        pygame.draw.circle(screen, self.color, pos, 5)

    def rescale(self, scale):
        self.x *= scale[0]
        self.y *= scale[1]
        self.update()

    def toDict(self, SCREEN_DIM):
        x = self.x / SCREEN_DIM[0]
        x = 0 if x < 0 else x
        x = 1 if x > 1 else x

        y = (self.y - SCREEN_DIM[1]/8) / (SCREEN_DIM[1] - SCREEN_DIM[1]/8)
        y = 0 if y < 0 else y
        y = 1 if y > 1 else y

        return [x, y]


class Shape():
    def __init__(self):
        self.rects = []
        self.starts = []
        self.ends = []

        self.line_color = [(255, 0, 0), (255, 150, 0), (255, 255, 0), (150, 255, 0), (0, 255, 0), (0, 255, 150), (
            0, 255, 255), (0, 150, 255), (0, 0, 255), (150, 0, 255), (255, 0, 255), (255, 0, 150)]

    def draw(self, screen):
        # draw rects
        for rect in self.rects:
            rect.draw(screen)

        # draw lines
        for i in range(len(self.starts)):
            start = self.starts[i]
            end = self.ends[i]
            xy_start = (self.rects[start].rect.x + self.rects[start].dim/2,
                        self.rects[start].rect.y + self.rects[start].dim/2)
            xy_end = (self.rects[end].rect.x + self.rects[start].dim/2,
                      self.rects[end].rect.y + self.rects[start].dim/2)
            pygame.draw.line(
                screen, self.line_color[self.id % len(self.line_color)], xy_start, xy_end, 3)

    def checkCollision(self, event_pos):
        for rect in self.rects:
            rect.collidepoint(event_pos)

    def mouseUp(self):
        for rect in self.rects:
            rect.draging = False

    def move(self, event_pos):
        for rect in self.rects:
            rect.move(event_pos)

    def getPosition(self):
        return (self.rects[0].rect.x, self.rects[0].rect.y)

    def toDict(self, SCREEN_DIM):
        data = []
        for rect in self.rects:
            data.append(rect.toDict(SCREEN_DIM))
        return data

    def fromDict(self, positions, SCREEN_DIM):
        for i in range(len(positions)):
            pos = positions[i]
            self.rects[i].x = pos[0] * SCREEN_DIM[0]
            self.rects[i].y = pos[1] * \
                (SCREEN_DIM[1] - SCREEN_DIM[1]/8) + SCREEN_DIM[1]/8
            self.rects[i].update()

    def rescale(self, old_dim, new_dim):
        scale = [new_dim[0] / old_dim[0], new_dim[1] / old_dim[1]]
        for rect in self.rects:
            rect.rescale(scale)

        #               4.___________________.5
        #               /|                  /|
        #          /     |              /    |
        #     /          |       /           |
        #   0.__________________.1           |
        #    |           |      |            |
        #    |           |      |            |
        #    |           |      |            |
        #    |          7.__________________.6
        #    |          /        |          /
        #    |      /            |       /
        #    | /                 |   /
        #   3.__________________.2


RECT_DIM = 15


class Cube(Shape):
    def __init__(self, pos, cube_id):
        super().__init__()
        offsets = [(0, 0), (100, 0), (100, 100), (0, 100),
                   (50, -50), (150, -50), (150, 50), (50, 50)]
        colors = [(255, 0, 0), (255, 150, 0), (255, 255, 0),
                  (150, 255, 0), (0, 255, 150), (0, 255, 255),
                  (0, 0, 255), (255, 0, 255)]

        self.id = cube_id

        self.starts = []
        self.ends = []
        for i in range(4):
            self.starts.append(i)
            self.ends.append(i + 1) if i < 3 else self.ends.append(0)
            self.starts.append(i + 4)
            self.ends.append(i + 5) if i < 3 else self.ends.append(4)
            self.starts.append(i)
            self.ends.append(i + 4) if i < 3 else self.ends.append(7)

        for index in range(len(offsets)):
            offset = offsets[index]
            rect_pos = (pos[0] + offset[0],
                        pos[1] + offset[1])
            color = colors[index]
            rect = Rect(rect_pos, RECT_DIM, color)
            self.rects.append(rect)


class Square(Shape):
    def __init__(self, pos, square_id):
        super().__init__()
        self.rects = []
        self.related_rects = [[3, 1], [2, 0], [1, 3], [0, 2]]
        offsets = [(0, 0), (100, 0), (100, 100), (0, 100)]
        colors = [(255, 0, 0), (255, 150, 0), (255, 255, 0),
                  (150, 255, 0)]
        self.id = square_id

        self.starts = []
        self.ends = []
        for i in range(4):
            self.starts.append(i)
            self.ends.append(i + 1) if i < 4 - 1 else self.ends.append(0)

        for index in range(len(offsets)):
            offset = offsets[index]
            rect_pos = ((pos[0] + offset[0]),
                        (pos[1] + offset[1]))
            color = colors[index]
            rect = Rect(rect_pos, RECT_DIM, color)
            self.rects.append(rect)

        self.static = False

    def checkCollision(self, event_pos):
        self.old_pos = (0, 0)
        for rect in self.rects:
            old_pos = rect.collidepoint(event_pos)
            if old_pos is not None:
                self.old_pos = old_pos

    def move(self, event_pos):
        if not self.static:
            for i in range(len(self.rects)):
                rect = self.rects[i]
                if rect.draging:
                    relatives = self.related_rects[i]
                    new_pos = rect.move(event_pos)
                    offset = (new_pos[0] - self.old_pos[0],
                              new_pos[1] - self.old_pos[1])
                    self.rects[relatives[0]
                               ].x = self.rects[relatives[0]].x + offset[0]
                    self.rects[relatives[0]].update()
                    self.rects[relatives[1]
                               ].y = self.rects[relatives[1]].y + offset[1]
                    self.rects[relatives[1]].update()
                    self.old_pos = new_pos
