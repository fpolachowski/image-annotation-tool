
from easydict import EasyDict
config = EasyDict()
config.OUTPUT_FILE = 'annotations.json'
config.FINAL_FILE = 'final_annotations.json'
config.TEMP_FILE = 'progress.json'
config.TEMP_USER = 'state.json'
config.FOLDER = 'images/'


service = EasyDict()
config.SERVICE = service
service.URL = 'https://vision.cs.princeton.edu/projects/2012/SUNprimitive/data_release.zip'
service.TARGETS = ['data_release/cuboid/Images/internet/*/*.jpg',
                   'data_release/cuboid/Images/misc/hand_captured/*.jpg']
service.RM_FOLDERS = ['__MACOSX', 'data_release']
service.RM_FILES = ['data_release.zip']

main = EasyDict()
config.MAIN = main
main.SCREEN = (800, 800)
main.WHITE = (255, 255, 255)
main.RED = (255,   0,   0)
main.FPS = 30


shapes = EasyDict()
config.SHAPES = shapes
shapes.SQUARE_DIM = 10
shapes.OFFSET = (13, 17)
shapes.OFFSETS = [(0, 0), (100, 0), (100, 100), (0, 100),
                  (50, -50), (150, -50), (150, 50), (50, 50)]
shapes.COLORS = [(255, 0, 0), (255, 150, 0), (255, 255, 0),
                 (150, 255, 0), (0, 255, 150), (0, 255, 255),
                 (0, 0, 255), (255, 0, 255)]
shapes.BUTTON_SQUARE = (40, 40)
shapes.BUTTON_LONG = (120, 40)


def update(image_dim):
    main.SCREEN = image_dim
    positions = EasyDict()
    config.POSITIONS = positions
    positions.BUTTON_OPEN = (main.SCREEN[0] / 16, main.SCREEN[1] / 20 - 45)
    positions.BUTTON_PLUS = (
        positions.BUTTON_OPEN[0] + shapes.BUTTON_LONG[0] + 10,  main.SCREEN[1] / 20)
    positions.BUTTON_MINUS = (
        positions.BUTTON_PLUS[0] + shapes.BUTTON_LONG[0] - shapes.BUTTON_SQUARE[0], main.SCREEN[1] / 20)
    positions.BUTTON_PREV = (main.SCREEN[0] / (8/5), main.SCREEN[1] / 20)
    positions.BUTTON_NEXT = (
        positions.BUTTON_PREV[0] + shapes.BUTTON_LONG[0] + 10, main.SCREEN[1] / 20)
    positions.BUTTON_COMMENT = (
        positions.BUTTON_PREV[0] - shapes.BUTTON_LONG[0] - 10, main.SCREEN[1] / 20 - 45)
    positions.BUTTON_EXIT = (
        positions.BUTTON_PREV[0] + shapes.BUTTON_LONG[0] + 10, main.SCREEN[1] / 20 - 45)
    positions.BUTTON_SETTINGS = (
        positions.BUTTON_OPEN[0] + shapes.BUTTON_LONG[0] + 10, main.SCREEN[1] / 20 - 45)
    positions.BUTTON_RESET = (
        positions.BUTTON_PREV[0], main.SCREEN[1] / 20 - 45)
    positions.LABEL_CUBES = (
        main.SCREEN[0] / 16 + shapes.BUTTON_LONG[0] / 2, main.SCREEN[1] / 20 + shapes.BUTTON_LONG[1] / 2)
    positions.LABEL_STATUS = (
        main.SCREEN[0] / 4 - 5, main.SCREEN[1] / 20 + shapes.BUTTON_LONG[1] / 2)
    positions.IMAGE = (0, main.SCREEN[1] / 8)
    positions.SQUARE = (main.SCREEN[0] / 2, main.SCREEN[1] / 4)
    positions.CUBES = (main.SCREEN[0] / 4, main.SCREEN[1] / 4)


update(main.SCREEN)


def getConfig():
    return config
