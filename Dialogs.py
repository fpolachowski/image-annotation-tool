import tkinter as tk
from tkinter.filedialog import askdirectory, askopenfilenames
import time

from Service import Service


class Dialog():
    def __init__(self):
        self.root = tk.Tk()

    def destroy(self):
        self.root.destroy()


class DownloadDialog(Dialog):
    def __init__(self):
        super().__init__()
        self.root.state('normal')
        self.root.protocol("WM_DELETE_WINDOW", self.destroy)
        self.root.title("Download images")
        self.root.geometry("400x240")

        self.service = None
        self.status = 'waiting'
        self.btnClose = tk.Button(self.root, height=1, width=10, text="Finish",
                                  command=self.destroy)

        self.msg_status = tk.Label(
            self.root, text='Waiting for download.')
        self.msg_status.pack()
        self.msg_progress = tk.Label(
            self.root, text='Please start the download.')
        self.msg_progress.pack()
        self.msg_percent = tk.Label(
            self.root, text='0 %')

        self.btnStart = tk.Button(self.root, height=1, width=15, text="Start download",
                                  command=self.startDownLoad)

        self.btnStart.pack()
        self.root.mainloop()

    def startDownLoad(self):
        self.btnStart.pack_forget()
        self.msg_percent.pack()
        self.service = Service()
        self.service.waiting = False
        self.updateLabel()

    def updateLabel(self):
        done = self.service.done
        status = self.service.status
        if(status != 'finished'):
            self.msg_progress['text'] = "\r[%s%s]" % (
                '|' * done, '-' * (50-done))
            self.msg_status['text'] = status
            self.msg_percent['text'] = '%s %% done' % (done * 2)
            self.root.after(100, self.updateLabel)
        if(status == 'finished'):
            self.msg_progress['text'] = 'Finished...'
            self.status = status
            self.btnClose.pack()

    def destroy(self):
        if self.service is not None:
            self.service.error = True
        self.root.destroy()


class CommentDialog(Dialog):
    def __init__(self, message=None):
        super().__init__()
        self.root.state('normal')
        self.root.protocol("WM_DELETE_WINDOW", self.destroy)
        self.root.title("Comment Dialog")

        self.root.geometry("400x240")

        self.text = tk.Text(self.root, height=10)
        self.text.pack()

        self.btnRead = tk.Button(self.root, height=1, width=10, text="Save",
                                 command=self.getTextInput)

        self.btnRead.pack()

        if message is not None:
            self.text.insert('end', message)

        self.message = message

        self.running = True
        self.root.mainloop()

    def getTextInput(self):
        self.message = self.text.get(1.0, tk.END+"-1c")
        self.destroy()

    def isRunning(self):
        return self.running

    def getText(self):
        return self.message


class FolderChooser(Dialog):

    # This is where we lauch the file manager bar.
    def OpenFile(self, path):
        return askdirectory(initialdir=path,
                            title="Choose a directory."
                            )


class FileChooser(Dialog):

    # This is where we lauch the file manager bar.
    def OpenFile(self, path, fileType=("JSON", ".json")):
        return askopenfilenames(initialdir=path, filetypes=[fileType])


class SettingsDialog(Dialog):
    def __init__(self, error=False):
        super().__init__()
        self.root.state('normal')
        self.root.protocol("WM_DELETE_WINDOW", self.destroy)
        self.root.title("Editor Dialog")

        self.root.geometry("150x70")

        if error:
            self.label = tk.Label(
                self.root, text="Please enter a number:", fg="red")
        else:
            self.label = tk.Label(
                self.root, text="Please enter your editor id:")
        self.label.pack()

        self.text = tk.Text(self.root, height=1)
        self.text.pack()

        self.btnRead = tk.Button(self.root, height=1, width=10, text="Save",
                                 command=self.getTextInput)

        self.btnRead.pack()

        self.message = None

        self.running = True
        self.root.mainloop()

    def getTextInput(self):
        self.message = self.text.get(1.0, tk.END+"-1c")
        self.destroy()

    def getText(self):
        return self.message
